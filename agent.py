import math
import os
import sys


class Agent():

    def __init__(self, ide=0, speed=0.0, acceleration=0.0, position=0.0, nextAcc=0.0, typeofCar=31):
        self.id = ide
        self.speed = speed
        self.acc = acceleration
        self.pos = position
        self.nextAcc = nextAcc
        self.typeofCar = typeofCar  # 31, 32, 41, 42


class AgentList():

    def __init__(self, number):
        self.Agents = [Agent(ide=i) for i in range(0, number)]
        self.LimitSpeed = 90
        self.MaxSpeed = 100
        self.SafeDistance = 50
        self.MaxAcc = 3
        self.MinAcc = -7.35
        self.Infinity = 200000.0
        self.Length = 12900.0
        self.Frequency = 50000.0

    def GetFrontDist(self, id):
        if id == 0:
            return self.Infinity
        else:
            return self.Agents[id].pos - self.Agents[id - 1].pos

    def GetBackDist(self, id):
        if id == len(self.Agents) - 1:
            return self.Infinity
        else:
            return self.Agents[id + 1].pos - self.Agents[id].pos

    def MakeDecision(self, id, DeepBrake=False):
        if DeepBrake:
            self.Agents[id].nextAcc = self.MinAcc
            return
        if self.GetFrontDist(id) > self.SafeDistance:
            if self.Agents[id].speed < self.MaxSpeed:
                self.Agents[id].nextAcc = self.MaxAcc
            else:
                self.Agents[id].nextAcc = 0
        elif self.GetFrontDist(id) == self.SafeDistance:
            self.Agents[id].nextAcc = self.Agents[id - 1].acc
        else:
            if self.Agents[id].acc < 0:
                # v2 ^2 = v1 ^ 2 + 2ax
                frontCarStopDist = (0 - self.Agents[id - 1].speed ** 2) / (2 * self.MinAcc)
                StopDist = frontCarStopDist + self.GetFrontDist(id) - 10
                self.Agents[id].nextAcc = max((0 - self.Agents[id].speed ** 2) / (2 * StopDist), self.MinAcc)

            elif self.Agents[id].speed < self.Agents[id - 1].speed:
                self.Agents[id].nextAcc = 0
            else:
                self.Agents[id].nextAcc = min(self.Agents[id - 1].acc - 0.5, 0)

    def UpdateAll(self):
        for i in range(0, len(self.Agents)):
            v0 = self.Agents[i].speed / 3.6
            a = self.Agents[i].nextAcc
            t = 1.0 / self.Frequency
            self.Agents[i].pos = self.Agents[i].pos - max((v0 * t + 0.5 * a * t * t), 0.0)
            self.Agents[i].speed = max(self.Agents[i].speed + (a * t), 0.0)
            self.Agents[i].acc = self.Agents[i].nextAcc

# Update position
# Update last Decision
# Update speed
