#!/usr/bin/env python3


import agent
import random

tunnel_legth = 12900.0
car_length = 4.7
bus_length = 12.0
frequency = 50000.0
break_rate = 0.0004  # [0.0, 1.0)
random.seed(3578)


def ground_truth_simulate(log):
    avg_velocity = float(log[1])
    print('Passing time for ground truth:\t{:.2f} s'.format((tunnel_legth + car_length) / (avg_velocity * 1000 / 3600)))


def random_break(num_vehicle):
    break_or_not = random.random()
    if break_or_not < break_rate:
        return random.randint(0, num_vehicle - 1)
    else:
        return -1


def multiagent_simulate(vehicle_list, log):
    num_vehicle = int(log[0])
    avg_velocity = float(log[1])
    avg_distance = float(log[2])
    time_counter = 0
    passing_time = 0.0
    last_car_start = True
    break_id = -1
    break_timer = 0.0
    for vehicle in vehicle_list.Agents:
        vehicle.speed = avg_velocity
        vehicle.pos = (vehicle.id - num_vehicle - 1) * avg_distance
    print('Start simulation')
    while True:
        time_counter += 1
        if break_id == -1:
            break_id = random_break(num_vehicle)
            if break_id != -1:
                break_timer = 1
                print('Car #{} deepbreak!'.format(break_id))
        else:
            break_timer += 1

        for vehicle in vehicle_list.Agents:
            if vehicle.speed != 0 or vehicle.pos != 0.0:
                if vehicle.id == break_id:
                    vehicle_list.MakeDecision(vehicle.id, True)
                else:
                    vehicle_list.MakeDecision(vehicle.id, False)
        vehicle_list.UpdateAll()

        if time_counter == frequency:
            time_counter = 0
            if last_car_start:
                passing_time += 1.0
        if break_timer >= frequency * 3.4:
            break_id = -1
            break_timer = 0.0
            print('Release break!')

        if vehicle_list.Agents[num_vehicle - 1].pos <= -1 * tunnel_legth:
            break
    passing_time += time_counter / frequency
    print('Passing time for simulation:\t{:.2f} s'.format(passing_time))


def read_log(log_list):
    with open('log.csv', 'r') as log_file:
        for line in log_file:
            line = line[:-1]
            log_list.append(line.split(','))


def main():
    log_list = []
    read_log(log_list)
    for line in log_list:
        print(
            'num_vehicle = {}, avg_velocity = {} km/hr, avg_distance = {:4f} m'.format(line[0], line[1], float(line[2])))
        vehicle_list = agent.AgentList(int(line[0]))
        multiagent_simulate(vehicle_list, line)
        ground_truth_simulate(line)
        print()


if __name__ == '__main__':
    main()
